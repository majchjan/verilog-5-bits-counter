# Verilog 5-bits counter

Prosty projekt testowy, licznik 5 bitowy w języku Verilog wraz z TestBenchem

## Kod:

        module counter_5bits(input clock,
                            input reset,
                            output reg [4:0] count); //licznik 5-bitowy
            always@(posedge reset or posedge clock) 
            begin
                if(reset) begin
                    count <= 5'b00000;
                end
                
                else if(count == 5'b11111) begin
                    count <= 5'b00000;
                end
                
                else begin
                    count <= count + 1;
                end
            end
        endmodule


        module counter_5bits_ts(); //Tester licznika
            reg clk;
            reg rst;
            wire [4:0] count;
            
            counter_5bits test(.clock(clk),
                                .reset(rst),
                                .count(count));
            
            initial
                begin
                    clk = 0; //Początkowe ustawienie zegara
                    rst = 1; //Początkowe resetowanie licznika
                    #10 rst = 0; // Po 10 jednostkach "zwolnij" RESET
                end
                
            always 
                begin
                    #1000000 clk = ~clk; //Po 1000000 jednostkach zmien stan 'clk' na przeciwny
                    if(clk) begin
                        $display("Count: ", count); //Wyswietl zawartosc 'count' gdy zegar osiągnie wartosc '1'
                    end
                end
        endmodule

# Verilog Watch

Prosty zegarek odliczający sekundy, minuty, godziny.

## Kod:
    module realClock(input reset,
                        input clock,
                        output reg [5:0] seconds,
                        output reg [5:0] minutes,
                        output reg [4:0] hours);
        initial
            begin
                seconds <= 6'b000000;
                minutes <= 6'b000000;
                hours <= 5'b00000;
            end
        always@(posedge reset or posedge clock)/* f sygnału zegarowego opcjonalna */
            begin
                if(reset) begin
                    seconds <= 6'b000000;
                    minutes <= 6'b000000;
                    hours <= 5'b00000;
                end
                else
                    seconds = seconds + 1;
                    if(seconds == 6'b111100) begin
                        seconds <= 6'b000000;
                        minutes <= minutes + 1;
                    end
                
                    if(minutes == 6'b111100) begin
                        minutes <= 6'b000000;
                        hours <= hours + 1;
                    end
                    if(hours == 5'b11000) begin
                        hours <= 5'b00000;
                    end
            end
    endmodule

    module realClock_tb(); //Tester zegarka
        reg rst;
        reg clk; //Jednostka czasu dowolna
        wire [5:0] secs;
        wire [5:0] mins;
        wire [4:0] hs;
        
        realClock test(.reset(rst),
                    .clock(clk),
                    .seconds(secs),
                    .minutes(mins),
                    .hours(hs));
        initial 
            begin
                clk = 1'b0;
                rst = 1'b1;
                #10 rst = 1'b0;
            end
        always 
            begin
                clk = ~clk;
                if(clk) begin
                    $display("%d : %d : %d", hs, mins, secs); //Wyswietl godzine
                end
            end
        

    endmodule
