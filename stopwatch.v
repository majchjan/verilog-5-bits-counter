module realClock(input reset,
					input clock,
					output reg [5:0] seconds,
					output reg [5:0] minutes,
					output reg [4:0] hours);
	initial
		begin
			seconds <= 6'b000000;
			minutes <= 6'b000000;
			hours <= 5'b00000;
		end
	always@(posedge reset or posedge clock)/* f sygnału zegarowego opcjonalna */
		begin
			if(reset) begin
				seconds <= 6'b000000;
				minutes <= 6'b000000;
				hours <= 5'b00000;
			end
			else
				seconds = seconds + 1;
				if(seconds == 6'b111100) begin
					seconds <= 6'b000000;
					minutes <= minutes + 1;
				end
			
				if(minutes == 6'b111100) begin
					minutes <= 6'b000000;
					hours <= hours + 1;
				end
				if(hours == 5'b11000) begin
					hours <= 5'b00000;
				end
		end
endmodule

module realClock_tb(); //Tester zegarka
	reg rst;
	reg clk; //Jednostka czasu dowolna
	wire [5:0] secs;
	wire [5:0] mins;
	wire [4:0] hs;
	
	realClock test(.reset(rst),
				.clock(clk),
				.seconds(secs),
				.minutes(mins),
				.hours(hs));
	initial 
		begin
			clk = 1'b0;
			rst = 1'b1;
			#10 rst = 1'b0;
		end
	always 
		begin
			clk = ~clk;
			if(clk) begin
				$display("%d : %d : %d", hs, mins, secs); //Wyswietl godzine
			end
		end
	

endmodule