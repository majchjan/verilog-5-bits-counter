module counter_5bits(input clock,
					input reset,
					output reg [4:0] count); //licznik 5-bitowy
	always@(posedge reset or posedge clock) 
	begin
		if(reset) begin
			count <= 5'b00000;
		end
		
		else if(count == 5'b11111) begin
			count <= 5'b00000;
		end
		
		else begin
			count <= count + 1;
		end
	end
endmodule


module counter_5bits_ts(); //Tester licznika
	reg clk;
	reg rst;
	wire [4:0] count;
	
	counter_5bits test(.clock(clk),
						.reset(rst),
						.count(count));
	
	initial
		begin
			clk = 0; //Początkowe ustawienie zegara
			rst = 1; //Początkowe resetowanie licznika
			#10 rst = 0; // Po 10 jednostkach "zwolnij" RESET
		end
		
	always 
		begin
			#1000000 clk = ~clk; //Po 1000000 jednostkach zmien stan 'clk' na przeciwny
			if(clk) begin
				$display("Count: ", count); //Wyswietl zawartosc 'count' gdy zegar osiągnie wartosc '1'
			end
		end
endmodule